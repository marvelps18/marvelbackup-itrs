<probes>
    <probe name="ITRS">
        <hostname>localhost</hostname>
        <port>55904</port>
    </probe>
    <probe name="TestAppServer">
        <hostname>testserver.training.local</hostname>
        <port>7037</port>
    </probe>
    <probe name="ProdAppServer">
        <hostname>prodserver.training.local</hostname>
        <port>7037</port>
    </probe>
    <probe name="ELK">
        <hostname>elk.training.local</hostname>
        <port>7037</port>
    </probe>
    <probe name="uDeploy">
        <hostname>uDeploy.training.local</hostname>
        <port>7037</port>
    </probe>
    <probe name="Docker">
        <hostname>dockerreg.training.local</hostname>
        <port>7037</port>
    </probe>
    <probe name="TeamCity">
        <hostname>teamcity.training.local</hostname>
        <port>7037</port>
    </probe>
    <probe name="ActiveMQ">
        <hostname>activemq.training.local</hostname>
        <port>7037</port>
    </probe>
    <probe name="TeamCity Agent">
        <hostname>172.31.32.204</hostname>
        <port>7037</port>
    </probe>
    <probe name="MySQL and Mongo">
        <hostname>mysql.training.local</hostname>
        <port>55902</port>
    </probe>
    <probe name="TeamCity Agent 2">
        <hostname>172.31.35.200</hostname>
        <port>7037</port>
    </probe>
    <probe name="Oracle">
        <hostname>oracle.training.local</hostname>
        <port>7037</port>
    </probe>
    <probe name="Ansible">
        <hostname>172.31.22.122</hostname>
        <port>7037</port>
    </probe>
</probes>